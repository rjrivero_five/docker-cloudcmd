FROM node:4.0

WORKDIR /root
RUN npm install -g cloudcmd

EXPOSE 8000

VOLUME /opt

CMD [ "/usr/local/bin/cloudcmd", \
      "--root", "/opt", \
      "--port", "8000", "--no-auth" ]
